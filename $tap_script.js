/*This Script uses Node.js to run
You can execute directly from your terminal
More info on node.js on: https://nodejs.org/*/

//Call the module https
const httpTransport = require('https');

//I'm using callback to make sure the request is done before moving forward
(function(callback) {
    //Defines the necessary variables for the HTTP request
    const responseEncoding = 'utf8';
    //I found the path by accessing TAP website and using the console to check the request the site makes
    const httpOptions = {
        hostname: 'book.flytap.com',
        port: '443',
        path: '/air/WebServices/Availability/TAPAvailability.asmx/GetOutboundFlights?_l=en',
        method: 'POST',
        headers: {"X-Requested-With":"XMLHttpRequest","Content-Type":"application/json; charset=utf-8","Cookie":"ASP.NET_SessionId=pobdfzbg2t2r2f2bh3z4aodt; PointOfSale=market=IE&contract=TAPIE&l=en; R3AirStartCookie=departure=SAO&arrival=LIS&depDate=28.06.2019&depTime=&retDate=19.05.2019&retTime=&flightType=Single&depTimeType=&retTimeType=&paxAdt=2&paxChd=&paxInf=&paxSrc=&paxYth=&paxStu=&paxYad=&resident=&cabinClass=&sort=SortPrice&depRadius=&arrRadius=&maxStopsNumber=0&offerType=Standard&rooms=&TransactionDebug=_air_WebServices_Availability_TAPAvailability.asmx_GetOutboundFlights_2019_05_19_12_39"}
    };
    httpOptions.headers['User-Agent'] = 'node ' + process.version;

    const request = httpTransport.request(httpOptions, (res) => {
        let responseBufs = [];
        let responseStr = '';
        res.on('data', (chunk) => {
            if (Buffer.isBuffer(chunk)) {	
                responseBufs.push(chunk);
            }
            else {
                responseStr = responseStr + chunk;            
            }
        }).on('end', () => {
            responseStr = responseBufs.length > 0 ? 
                Buffer.concat(responseBufs).toString(responseEncoding) : responseStr;
            
            callback(null, res.statusCode, res.headers, responseStr);
        });
        
    })
    .setTimeout(0)
    .on('error', (error) => {
        callback(error);
    });
    
    //Makes sure the request is using the current day, if you use another day the request might fail
    var today = new Date();
    month = today.getMonth();
    day = today.getDate();
    todayDate = toDouble(day.toString()) + '.' + toDouble(month.toString()) + '.' + today.getFullYear();
    
    /*The following three fields are the only ones I'm using on the request
    and it's only one way flight, one person, and the first flight of the day
    we could personalize it more but for this exercise I decide to keep it simple
	Other routes:
			SAO-OPO,SAO-LIS*/
    flightOrigin = 'SAO';
    flightDest = 'OPO';
    flightDepDate = '06.07.2019';
    
    //TAP uses a POST request to render the website, I'm making this request directly so I don't have to scrap the website
    requestWriteStr = "{\"availabilityRequest\":{\"origin\":\""+flightOrigin+"\",\"destination\":\""+flightDest+"\",\"depDate\":\""+flightDepDate+"\",\"retDate\":\""+todayDate+"\",\"flightType\":\"Single\",\"adt\":\"2\",\"_l\":\"en\",\"contract\":\"TAPIE\",\"isB2B\":false}}";
    request.write(requestWriteStr);
    request.end();
    

})((error, statusCode, headers, body) => {
    //Prints the result of the request to check for errors
    console.log('--------TAP: Request results--------')
    console.log('ERROR:', error); 
    console.log('STATUS:', statusCode);
    //Extracts and send the data if there's no error
    if (statusCode == 200){
    	flightData = getFlightData(body);
    	dataToSend = extractFlightData(flightData);
    	dataToSend = JSON.stringify(dataToSend);
		//console.log(dataToSend);
		sendData(dataToSend);
		}
});

//Extracts the info of the first flight on the request
function getFlightData(body){
	body = body.replace(/\\/g, "");
 	n_start = body.indexOf('{"SegmentList":[{"Number":1');
    n_end = body.indexOf('{"SegmentList":[{"Number":1',n_start+1);
    body = body.substr(n_start,n_end-n_start);
    return body;
}

//Extracts the values of the fields present on the array
function extractFlightData(flightData){
	var dataJSON = [];
	fieldsNames = ['DisplayPrice','FlightNumber','AirlineName','DepartureDate','DepartureTime','FlightString','OriginTLC','DestinationTLC','OriginCountry','DestinationCountry']
	fieldsNames.forEach(function(name){
		dataJSON.push(pushExt(name,flightData));	
	});
	return dataJSON;
}

//Extraction function
function ext(fieldName,fullData){
	fieldStr = '"' + fieldName + '":';
	len = fieldStr.length;
	start = fullData.indexOf(fieldStr)+1;
	end = fullData.indexOf(',',start + 1);
	return fullData.substr(start + len, end-start-len-1);
}

//Function to add the data to array
function pushExt(fieldName,flightData){
	return {fieldName:fieldName, value:ext(fieldName,flightData)};
}

//Function to make sure the value has 2 digits
function toDouble(valueStr){
	if(valueStr.length<2){
		return '0'+valueStr;
	}
	return valueStr;
}

//Send data to api.capture.ie
function sendData(dataToSend){
	(function(callback) {
	    //Defines the necessary variables for the HTTP request
	    const responseEncoding = 'utf8';
	    //I found the path by accessing TAP website and using the console to check the request the site makes
	    const httpOptions = {
	        hostname: 'api.capturedata.ie',
	        method: 'POST',
	        headers: {"X-Requested-With":"XMLHttpRequest","Content-Type":"application/json; charset=utf-8"}
	    };
	    httpOptions.headers['User-Agent'] = 'node ' + process.version;

	    const request = httpTransport.request(httpOptions, (res) => {
	        let responseBufs = [];
	        let responseStr = '';
	        res.on('data', (chunk) => {
	            if (Buffer.isBuffer(chunk)) {	
	                responseBufs.push(chunk);
	            }
	            else {
	                responseStr = responseStr + chunk;            
	            }
	        }).on('end', () => {
	            responseStr = responseBufs.length > 0 ? 
	                Buffer.concat(responseBufs).toString(responseEncoding) : responseStr;
	            
	            callback(null, res.statusCode, res.headers, responseStr);
	        });
	        
	    })
	    .setTimeout(0)
	    .on('error', (error) => {
	        callback(error);
	    });
	    request.write(dataToSend);
	    request.end();
	    

	})((error, statusCode, headers, body) => {
	    //Prints the result of the request to check for errors
	    console.log('--------Request results--------')
	    console.log('ERROR:', error); 
	    console.log('STATUS:', statusCode);
	});
}
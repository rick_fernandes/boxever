#Necessary modules for the code to work
import json
import requests

#Function to validate split line
def validateSplitLine(line):
	conditions = True
	if conditions:
		return True
	return False

#Function to split the line into columns
def splitLine(line):
	line = line.replace(',','').replace('\n','')
	line = line.split(';')
	return line

#Function to validate the line
def validateLine(line):
	conditions = True
	if conditions:
		return True
	return False

#Function that will add the line to data if the line is valid
def addLine(data,line):
	if validateLine(line):
		line = splitLine(line)
		if validateSplitLine(line):
			data.append(line)

#Function to read lines and return a 2D list
def readLines(filename):
	data=[]
	with open(filename) as fp:
		line = fp.readline()
		addLine(data,line)
		while line:
			line = fp.readline()
			addLine(data,line)
	return data

#Function to create an Object array
def createTripArray(data):
	tripArray = []
	del data[len(data)-1]
	del data[0]
	for d in data:
		tripArray.append(Trip(d[0],d[1],d[2],d[3],d[4],d[5]))
	return tripArray

#Function to convert an Object to a string
def tempTrip(vTrip):
	tempStr = '{"from":"'+vTrip.vFrom+'","to":"'+vTrip.vTo+'","name":"'+vTrip.vName+'","date":"'+vTrip.vDate+'","flight":"'+vTrip.vFlight+'","passport":"'+vTrip.vPassport+'"},'
	return tempStr

#Function to create an array of string containing the trips
def createJSONarray(tripArray):
	json_string = '['
	for t in tripArray:
		json_string = json_string + tempTrip(t)
	json_string = json_string[:-1]
	json_string = json_string + ']'
	return json_string

#Function to send the data
def send_request(dataToSend):
    try:
        response = requests.post(
            url="api.capturedata.ie​",
            params={},
            headers={
                "Content-Type": "application/json"
            },
            data=json.dumps(dataToSend)
        )
        print('Response HTTP Status Code: {status_code}'.format(
            status_code=response.status_code))
        print('Response HTTP Response Body: {content}'.format(
            content=response.content))
    except requests.exceptions.RequestException:
        print('HTTP Request failed')

#Defines a class with the trip fields
class Trip:
	def __init__(self,vFrom,vTo,vName,vDate,vFlight,vPassport):
		self.vFrom = vFrom
		self.vTo = vTo
		self.vName = vName
		self.vDate = vDate
		self.vFlight = vFlight
		self.vPassport = vPassport

#Starts the code from here
if __name__ == '__main__':
	file = 'input_data.csv'
	print(file + '-- Started')
	data = readLines(file)
	tripArray = createTripArray(data)
	tripArrayJSON = createJSONarray(tripArray)
	jsonData = json.loads(tripArrayJSON)
	print(file + '-- Read')
	print('Sending data...')
	send_request(jsonData)
	#Write to file
	with open('data.json','w') as f:
		json.dump(jsonData,f)